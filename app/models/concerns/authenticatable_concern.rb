module AuthenticatableConcern
  extend ActiveSupport::Concern

  included do
    ## Devise Rollout Phase replications
    #
    # Will need to be removed in second Devise rollout phase

    # the following attributes should be inherited from
    # Person, but for some unknown reason aren't
    attr_accessor :password, :password_confirmation

    belongs_to :practice
    has_one :user, inverse_of: :person, foreign_key: 'person_id', dependent: :destroy
    accepts_nested_attributes_for :user

    ## Delegations
    #
    # Needed in order to replicate functionality for calls on legacy login functionality
    delegate :is_role?, to: :user

    def authenticatable?
      true
    end
  end
end
