require 'digest/sha1'

class Person < ApplicationRecord
  CANTON = %w[
    AG AI AR
    BE BL BS
    FR
    GE GL GR
    JU
    LU
    NE NW
    OW
    SG SH SO SZ
    TI TG
    UR
    VD VS
    ZG ZH
    LI
    A D F I
  ].freeze

  def self.valid_cantons
    [''] + CANTON
  end

  ROLES = {
    1 => 'admin',
    2 => 'bookkeeper',
    5 => 'assistant',
    7 => 'physician'
  }.freeze

  CALENDAR_DEFAULT_VIEW_TYPES = %w[day week work_days month physician resource event_type].freeze

  ALLOWED_VALUES = {
    salutation: %w[mr ms],
    sex: %w[m f],
    civil_status: %w[married single child widowed divorced],
    title: ['Dr. med. dent.', 'Med. dent.', 'DentalhygienikerIn', 'ProphylaxeassistentIn'],
    lang: %w[de fr it en other],
    calendar_default_view: CALENDAR_DEFAULT_VIEW_TYPES
  }.freeze

  enum calendar_default_view: CALENDAR_DEFAULT_VIEW_TYPES

  validates :email, length: { within: 6..100, allow_blank: true }
  validates :email, format: { with: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i, allow_blank: true }

  validates        :ean, :ean2, format: { with: /\A[0-9]{13}\z/, allow_blank: true }
  validates        :zsr,        format: { with: /\A[A-Z][0-9]{6}\z/, allow_blank: true }
  validates        :ssn,        format: { with: /\A([0-9]{4,10}|[1-9][0-9]{10}|756[0-9]{10}|438[0-9]{10})+$\z/, allow_blank: true },
                                length: { is: 13 }, if: :rebuild_ssn?
  validates :canton, inclusion: valid_cantons, allow_nil: true
  validates :calendar_default_view, inclusion: { in: Person.calendar_default_views },
                                    presence: true, if: proc { |obj| obj.has_attribute?(:calendar_default_view) }

  has_attached_file :picture,
                    styles: { logo: ['400x200', :png], thumb: ['100x100>', :png], full_logo: ['2000x400>', :png] },
                    convert_options: { logo: '-gravity northeast -extent 400x200' },
                    path: ":rails_root/#{Rails.env.test? ? 'tmp/pp' : 'private'}/:class/:id_partition/:style/:filename"
  do_not_validate_attachment_file_type :picture

  after_initialize :initialize_fields
  before_validation :extract_ssn, if: :rebuild_ssn?
  after_validation :prepare_ssn, if: :rebuild_ssn?
  before_create :generate_display_name

  NAME_MATCHES_SQL = <<-SQL.squish
      to_tsvector('simple', coalesce(people.name, '')) || to_tsvector('simple', coalesce(people.prename, '')) || to_tsvector('simple', coalesce(people.company, ''))
    @@
      to_tsquery('simple', :tsquery_string)
  SQL

  scope :perfect_no_match, ->(query) { where(no: query) }
  scope :prefix_no_matches, ->(regexp) { where('people.no ~* ?', "^[0 ]*(#{regexp})") }
  scope :skipped_prefix_no_matches, ->(regexp) { where('people.no ~* ?', "^[[:alpha:]](#{regexp})") }
  scope :name_matches, ->(tsquery_string) { where(NAME_MATCHES_SQL, tsquery_string: tsquery_string) }

  scope :by_text_query, lambda { |query, regexp, tsquery_string, order|
    perfect_no_match(query)
      .or(prefix_no_matches(regexp))
      .or(name_matches(tsquery_string))
      .or(skipped_prefix_no_matches(regexp))
      .reorder(order)
  }

  def self.tsquery_string(tokens)
    tokens.map { |token| "(#{token} | #{token}:*)" }.join(' & ')
  end

  def self.by_text(query)
    return all if query.blank?

    tokens = query.gsub(/[^[:alnum:][ ]]/, '').split(' ')
    regexp = tokens.map { |token| Regexp.escape(token) }.join('|')
    order = Arel.sql('rank() OVER(ORDER BY length(people.no))')
    by_text_query(query, regexp, tsquery_string(tokens), order)
  end

  def self.query(params)
    by_text(params[:filter])
  end

  class AuthenticationError < StandardError; end
  class LazyDeviseMigrationError < StandardError; end

  def self.authenticate_login(login, password)
    raise(AuthenticationError, 'missing_login_or_password') if login.blank? || password.blank?
    user_person = find_by(login: login) || find_by(email: login)
    raise(AuthenticationError, 'wrong_password') unless user_person&.authenticated?(password) && !user_person.archived_at?
    user_person.migrate_to_user!(password)
  end

  def migrate_to_user!(password, fallback_email: 'suport+no-email-lazy-migrate@denteo.ch')
    raise(LazyDeviseMigrationError, 'not_a_legacy_user') unless legacy_user?
    raise(LazyDeviseMigrationError, 'allready_migrated_user') if migrated_user?

    self.email = fallback_email if email.blank?
    self.user = User.new(username: login, password: password, role: User.roles.invert[role], email: email)
    save!
    self
  end

  # TODO: Remove this after time scoped first devise lazy migration phase
  def legacy_user?
    crypted_password.present? && created_at < Time.zone.local(2019, 5, 3) # TODO: Add rollout deploy date as env variable
  end

  # TODO: Remove this after time scoped first devise lazy migration phase
  def migrated_user?
    legacy_user? && user.present?
  end

  def picture_thumb_path
    if picture.file?
      picture.path(:thumb)
    else
      Rails.root.join('public', 'pictures', 'thumb', 'missing.png')
    end
  end

  def initialize_fields
    self.lang ||= ENV['DENTEO_CLIENT_LOCALE'] || 'de'
    self.locale ||= ENV['DENTEO_CLIENT_LOCALE'] || 'de'
  end

  def login=(value)
    self[:login] = value.blank? ? nil : value.downcase
  end

  def email=(value)
    self[:email] = value.blank? ? nil : value.downcase
  end

  def password_required?
    false
  end

  def salutation_display(short = false)
    return nil if salutation.blank?
    scope = short ? %w[people short_salutation] : %w[people salutation]
    I18n.t(salutation, scope: scope)
  end

  # returns a printable representation used for address fields
  def company_and_doctor_name
    rows = [:company, :salutation_display, %i[title prename name]]
    rows = rows.map do |keys|
      [keys].flatten.map { |key| send(key) }.join(' ').normalize_ws
    end.reject(&:blank?)
    rows.join("\n")
  end

  def to_s
    display_name || ''
  end

  def to_address_block
    rows = [:company, :salutation_display, %i[title prename name],
            :street, :additional_address, %i[zip city]]
    rows = rows.map do |keys|
      [keys].flatten.map { |key| send(key) }.join(' ').normalize_ws
    end.reject(&:blank?)
    rows.join("\n")
  end

  def to_address_block_with_tel
    to_address_block + (phone.blank? ? '' : "\n#{phone}")
  end

  def as_mobile(value)
    number = value&.gsub(/[^\d+]/, '').presence
    return unless number

    return number if number.match(/^07/) || number.match(/^\+417/) || number.match(/^00417/)
  end

  def as_json(options)
    options[:except] ||= %i[
      password password_confirmation crypted_password
      practice_id type api_token
    ]
    super(options)
  end

  def sms
    # Try simplified mobile number, then phone
    as_mobile(mobile) || as_mobile(phone)
  end

  # here, have some examples ...
  #  Peter Mueller -> P. Mueller
  #  Peter        -> Peter
  #        Mueller -> Mueller
  #               ->
  def short_name
    if name.blank?
      prename.presence || ''
    else
      (prename.blank? ? '' : (prename.first + '.')) + name
    end
  end

  def sex
    { 'mr' => 'm', 'ms' => 'f' }.fetch(salutation, nil)
  end

  # some fields are only relevant for 'business' persons - this lets
  # the class answer if it is such an object or not (relevant for index page,
  # search field...)
  def self.business?
    false
  end

  def zip_and_city
    [zip, city].compact * ' '
  end

  def label_dob
    return '' unless dob
    I18n.localize(dob, format: :label)
  end

  def fullname
    [prename, name].compact * ' '
  end

  def display_name
    self[:display_name].presence || login
  end

  def display_name_with_salutation(short = false)
    salutation_display(short).to_s +
      (salutation_display.blank? ? '' : ' ') +
      display_name.to_s
  end

  def display_name_with_short_salutation
    display_name_with_salutation(true)
  end

  def informal_salutation
    return '' unless sex
    [I18n.t("activerecord.attributes.person.informal_salutation.#{sex}"), prename].join ' '
  end

  def formal_salutation
    return '' unless sex
    [I18n.t("activerecord.attributes.person.formal_salutation.#{sex}"), name].join ' '
  end

  def name_with_salutation
    [salutation_display, prename, name].map(&:presence).compact.map(&:strip).join ' '
  end

  def age
    ((Time.zone.today - dob) / 365).to_i if dob
  end

  def physician?
    false
  end

  def assistant?
    false
  end

  def authenticatable?
    false
  end

  def authenticated?(password)
    crypted_password == encrypt(password)
  end

  def self.make_token
    secure_digest(Time.zone.now, (1..10).map { rand.to_s })
  end

  def self.secure_digest(*args)
    Digest::SHA1.hexdigest(args.flatten.join('--'))
  end

  # This provides a modest increased defense against a dictionary attack if
  # your db were ever compromised, but will invalidate existing passwords.
  # See the README and the file config/initializers/site_keys.rb
  #
  # It may not be obvious, but if you set REST_AUTH_SITE_KEY to nil and
  # REST_AUTH_DIGEST_STRETCHES to 1 you'll have backwards compatibility with
  # older versions of restful-authentication.
  def self.password_digest(password, salt)
    digest = REST_AUTH_SITE_KEY
    REST_AUTH_DIGEST_STRETCHES.times do
      digest = secure_digest(digest, salt, password, REST_AUTH_SITE_KEY)
    end
    digest
  end

  def encrypt_password!
    return if password.blank?
    self.salt = self.class.make_token if new_record?
    self.crypted_password = encrypt(password)
    save!
  end

  protected

  def generate_display_name
    return self.display_name = company if self.class.business?
    self.display_name = "#{prename} #{name} #{no}".normalize_ws
  end

  # Encrypts the password with the user salt
  def encrypt(password)
    self.class.password_digest(password, salt)
  end

  def rebuild_ssn?
    ssn_changed? && ssn.present?
  end

  def extract_ssn
    self.ssn = ssn.gsub(/[.,' ']/, '')
  end

  def prepare_ssn
    updated_ssn = ssn
    length = ssn.length
    [3, 8, 13].each do |index|
      break if length < index
      updated_ssn.insert(index, '.')
    end
    self.ssn = updated_ssn
  end
end
