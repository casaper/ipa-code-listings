class User < ApplicationRecord
  attr_writer :login

  VALID_ROLES = {
    physician: %w[admin physician],
    assistant: %w[admin assistant bookkeeper]
  }.freeze

  devise :invitable, :database_authenticatable, :recoverable, :rememberable

  enum role: { admin: 1, bookkeeper: 2, assistant: 5, physician: 7 }

  belongs_to :person
  delegate :display_name, :name_with_salutation, :formal_salutation, :display_name_with_short_salutation,
           :fullname, :label_dob, :zip_and_city, :sex, :short_name, :sms, :to_address_block_with_tel,
           :to_address_block, :salutation_display, :locale, :practice_id, :physician?, :assistant?,
           :physician_id, :kind, :api_token, :lang,
           to: :person, allow_nil: true

  has_one :practice, through: :person
  has_many :events, through: :person
  has_many :business_hours, through: :person
  has_many :prescriptions, through: :person

  validates :username, uniqueness: { case_sensitive: false, allow_blank: true }
  validates :email, length: { within: 6..100 },
                    uniqueness: true,
                    presence: true,
                    format: {
                      with: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
                    }
  validates :role, inclusion: { in: VALID_ROLES[:physician] }, if: :physician?
  validates :role, inclusion: { in: VALID_ROLES[:assistant] }, if: :assistant?
  validates :person, presence: true
  validate :person_type_validation, if: :person_present?

  # rubocop:disable Naming/PredicateName
  # Emulates legacy authentication and User implementation
  def is_role?(value)
    role == value
  end
  # rubocop:enable Naming/PredicateName

  def login
    @login || username || email
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    login = conditions.delete(:login)
    if login
      where(conditions.to_h).find_by('lower(username) = :value OR lower(email) = :value', value: login.downcase)
    elsif conditions.key?(:username) || conditions.key?(:email)
      find_by(conditions.to_h)
    end
  end

  def roles_collection
    if physician?
      roles_t.slice(*VALID_ROLES[:physician].map(&:to_sym)).invert
    elsif assistant?
      roles_t.slice(*VALID_ROLES[:assistant].map(&:to_sym)).invert
    end
  end

  private

  def person_present?
    person.present?
  end

  def person_type_validation
    errors.add(:person, :wrong_type) unless person.authenticatable?
  end
end
