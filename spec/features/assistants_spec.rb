require 'rails_helper'

RSpec.describe 'Assistant', type: :feature do
  context 'Wenn ein Admin im Webformular' do
    describe 'eine neue Assistenz erstellt' do
      before do
        admin = create(:user_admin)
        sign_in admin
        visit new_assistant_path
      end

      it 'wird ein User für die Assistenz erstellt, und eine Benutzereinladung an die angegebene Email versendet' do
        fill_in 'E-Mail', with: 'tester.assistant@denteo.ch'
        fill_in 'Name', with: 'Johny Assistant'
        expect { click_on('Assistenz speichern') }.to change(User, :count).by(1)
        user = User.find_by(email: 'tester.assistant@denteo.ch')
        expect(user.invitation_sent_at).not_to be_nil
        expect(user.email).to eq user.person.email
      end
    end
  end
end
