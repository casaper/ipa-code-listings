require 'rails_helper'

RSpec.describe 'Physician', type: :feature do
  context 'Wenn ein Admin im Webformular' do
    describe 'einen neuen Behandler erstellt' do
      before do
        admin = create(:user_admin)
        sign_in admin
        visit new_physician_path
      end

      it 'wird ein User für den Behandler erstellt, und eine Benutzereinladung an die angegebene Email versendet' do
        fill_in 'E-Mail', with: 'tester.physician@denteo.ch'
        fill_in 'Name', with: 'Johny Physician'
        expect { click_on('Behandler speichern') }.to change(User, :count).by(1)
        user = User.find_by(email: 'tester.physician@denteo.ch')
        expect(user.invitation_sent_at).not_to be_nil
        expect(user.email).to eq user.person.email
      end
    end
  end
end
