require 'rails_helper'

RSpec.describe 'Authentifizierung', type: :feature do
  describe 'Das Login Formular' do
    before do
      create(:user_admin, username: 'try_login', password: 'correct_password', email: 'try.login@denteo.ch')
      visit root_path
    end

    it 'zeigt alle Felder und Buttons an' do
      expect(page).to have_current_path new_user_session_path
      expect(page).to have_field 'Emailadresse oder Login'
      expect(page).to have_field 'Passwort'
      expect(page).to have_button 'Weiter'
    end

    it 'kann einen Benutzer mit Benutzernamen und Passwort anmelden' do
      fill_in 'Emailadresse oder Login', with: 'try_login'
      fill_in 'Passwort', with: 'correct_password'
      click_button 'Weiter'
      expect(page).to have_content 'Erfolgreich angemeldet.'
      expect(page).to have_current_path root_path
    end

    it 'kann einen Benutzer mit Emailadresse und Passwort anmelden' do
      fill_in 'Emailadresse oder Login', with: 'try.login@denteo.ch'
      fill_in 'Passwort', with: 'correct_password'
      click_button 'Weiter'
      expect(page).to have_content 'Erfolgreich angemeldet.'
      expect(page).to have_current_path root_path
    end

    it 'meldet einen Benutzer mit falschem Passwort nicht an' do
      fill_in 'Emailadresse oder Login', with: 'try_login'
      fill_in 'Passwort', with: 'wrong password'
      click_button 'Weiter'
      expect(page).to have_content 'Login oder Passwort ungültig'
      expect(page).to have_current_path new_user_session_path
    end

    it 'meldet einen Benutzer mit ungültigem Benutzernamen nicht an' do
      fill_in 'Emailadresse oder Login', with: 'wrong login name'
      fill_in 'Passwort', with: 'correct_password'
      click_button 'Weiter'
      expect(page).to have_content 'Login Angaben sind falsch'
      expect(page).to have_current_path new_user_session_path
    end

    it 'meldet nicht an wenn kein Benutzername oder Email eingegeben wurde' do
      fill_in 'Passwort', with: 'correct_password'
      click_button 'Weiter'
      expect(page).to have_content 'Login/Email oder Passwort fehlt.'
      expect(page).to have_current_path new_user_session_path
    end

    it 'meldet nicht an wenn kein Passwort angegeben wurde' do
      fill_in 'Emailadresse oder Login', with: 'try_login'
      click_button 'Weiter'
      expect(page).to have_content 'Login oder Passwort ungültig'
      expect(page).to have_current_path new_user_session_path
    end

    it 'meldet nicht an wenn nichts eingegeben wurde' do
      click_button 'Weiter'
      expect(page).to have_content 'Login/Email oder Passwort fehlt.'
      expect(page).to have_current_path new_user_session_path
    end
  end

  context 'Wenn ein noch nicht migrierter Benutzer zum ersten mal einloggt' do
    let!(:physician) { create(:physician_legacy, login: 'legacy_physician', email: 'legacy@denteo.ch') }

    before { visit root_path }

    describe 'und einen korrekten Login-Namen und Passwort eingibt' do
      it 'wird ein User mit korrekter Rolle erstellt, dass richtige Passwort gesetzt und gleich angemeldet' do
        fill_in 'Emailadresse oder Login', with: 'legacy_physician'
        fill_in 'Passwort', with: 'Denteo12'
        expect { click_button 'Weiter' }.to change(User, :count).by(1)
        expect(physician.reload.user).to have_attributes username: 'legacy_physician', email: 'legacy@denteo.ch'
        expect(physician.user).to be_a_physician
        expect(physician.user.valid_password?('Denteo12')).to be true
      end
    end

    describe 'und einer korrekten Email und Passwort eingibt' do
      it 'wird ein User mit korrekter Rolle erstellt, dass richtige Passwort gesetzt und gleich angemeldet' do
        fill_in 'Emailadresse oder Login', with: 'legacy@denteo.ch'
        fill_in 'Passwort', with: 'Denteo12'
        expect { click_button 'Weiter' }.to change(User, :count).by(1)
        expect(physician.reload.user).to have_attributes username: 'legacy_physician', email: 'legacy@denteo.ch'
        expect(physician.user).to be_a_physician
        expect(physician.user.valid_password?('Denteo12')).to be true
      end
    end

    describe 'und ein falsches Passwort eingibt' do
      it 'wird nicht angemeldet und nicht migriert' do
        fill_in 'Emailadresse oder Login', with: 'legacy@denteo.ch'
        fill_in 'Passwort', with: 'wrong'
        expect { click_button 'Weiter' }.to change(User, :count).by(0)
        expect(page).to have_content 'Login Angaben sind falsch.'
        expect(page).to have_current_path new_user_session_path
      end
    end

    describe 'und kein Passwort angibt' do
      it 'wird nicht angemeldet und nicht migriert' do
        fill_in 'Emailadresse oder Login', with: 'legacy@denteo.ch'
        expect { click_button 'Weiter' }.to change(User, :count).by(0)
        expect(page).to have_content 'Login/Email oder Passwort fehlt.'
        expect(page).to have_current_path new_user_session_path
      end
    end

    describe 'und keinen Benutzernamen oder keine Emailadresse eingibt' do
      it 'wird nicht angemeldet und nicht migriert' do
        fill_in 'Passwort', with: 'Denteo12'
        expect { click_button 'Weiter' }.to change(User, :count).by(0)
        expect(page).to have_content 'Login/Email oder Passwort fehlt.'
        expect(page).to have_current_path new_user_session_path
      end
    end

    describe 'und weder Passwort noch Benutzername/Emailadresse eingibt' do
      it 'wird nicht angemeldet und nicht migriert' do
        expect { click_button 'Weiter' }.to change(User, :count).by(0)
        expect(page).to have_content 'Login/Email oder Passwort fehlt.'
        expect(page).to have_current_path new_user_session_path
      end
    end

    describe 'und einen ungültigen Benutzernamen eingibt' do
      it 'wird nicht angemeldet und nicht migriert' do
        fill_in 'Emailadresse oder Login', with: 'this person does not exist'
        fill_in 'Passwort', with: 'Denteo12'
        expect { click_button 'Weiter' }.to change(User, :count).by(0)
        expect(page).to have_content 'Login Angaben sind falsch.'
        expect(page).to have_current_path new_user_session_path
      end
    end
  end

  context 'Wenn ein per Anmelden bereits migrierter Benutzer sich erneut anmeldet' do
    let!(:physician) { create(:physician_legacy, login: 'legacy_physician', email: 'legacy@denteo.ch') }

    before do
      # Manually do the previous migrative login first
      visit root_path
      fill_in 'Emailadresse oder Login', with: 'legacy_physician'
      fill_in 'Passwort', with: 'Denteo12'
      click_button 'Weiter'
      page.find('.p-m-0.profile_link.dropdown-button[href="#"]').click
      click_link 'Abmelden'
      visit root_path
    end


    describe 'und den korrekten Benutzernamen und das Gültige Passwort eingibt' do
      it 'wird angemeldet, kein neuer User erstellt und auf den "root_path" weitergeleitet' do
        fill_in 'Emailadresse oder Login', with: 'legacy_physician'
        fill_in 'Passwort', with: 'Denteo12'
        expect { click_button 'Weiter' }.to change(User, :count).by(0)
        expect(page).to have_content 'Erfolgreich angemeldet.'
        expect(page).to have_current_path root_path
      end
    end

    describe 'und ein falsches Passwort eingibt' do
      it 'wird nicht angemeldet, kein neuer User erstellt und eine Fehlermeldung angezeigt' do
        fill_in 'Emailadresse oder Login', with: 'legacy_physician'
        fill_in 'Passwort', with: 'wrong password'
        expect { click_button 'Weiter' }.to change(User, :count).by(0)
        expect(page).to have_content 'Login oder Passwort ungültig'
        expect(page).to have_current_path new_user_session_path
      end
    end
  end
end
