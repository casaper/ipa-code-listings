require 'rails_helper'

RSpec.describe Assistant, type: :model do
  subject(:assistant) { Assistant.new }

  describe '#physician?' do
    it 'gibt "false" zurück' do
      expect(assistant.physician?).to be false
    end
  end

  describe '#assistant?' do
    it 'gibt "true" zurück' do
      expect(assistant.assistant?).to be true
    end
  end

  describe '#authenticatable?' do
    it 'gibt "true" zurück' do
      expect(assistant.authenticatable?).to be true
    end
  end
end
