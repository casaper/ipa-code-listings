require 'rails_helper'

RSpec.describe Guarantor, type: :model do
  describe '#physician?' do
    it 'gibt "false" zurück' do
      expect(Guarantor.new.physician?).to be false
    end
  end

  describe '#assistant?' do
    it 'gibt "false" zurück' do
      expect(Guarantor.new.assistant?).to be false
    end
  end

  describe '#authenticatable?' do
    it 'gibt "false" zurück' do
      expect(Guarantor.new.authenticatable?).to be false
    end
  end
end
