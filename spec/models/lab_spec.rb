require 'rails_helper'

RSpec.describe Lab, type: :model do
  describe '#physician?' do
    it 'gibt "false" zurück' do
      expect(Lab.new.physician?).to be false
    end
  end

  describe '#assistant?' do
    it 'gibt "false" zurück' do
      expect(Lab.new.assistant?).to be false
    end
  end

  describe '#authenticatable?' do
    it 'gibt "false" zurück' do
      expect(Lab.new.authenticatable?).to be false
    end
  end
end
