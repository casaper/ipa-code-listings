require 'rails_helper'

RSpec.describe Person, type: :model do
  # TODO: Remove this after time scoped first devise lazy migration phase
  context 'Wenn die erste Phase des Devise Migrations ausgerollt wurde' do
    let(:admin) { create(:admin, :legacy_user, login: 'admin_1', email: 'admin_1@example.com') }

    describe '#legacy_user?' do
      it 'ist true wenn die Behandlerin vor dem 3. Mai 2019 erstellt wurde und ein verschlüsseltes Passwort hat' do
        admin.update!(created_at: Time.zone.local(2019, 1, 1)) # actually the factory takes care of this, but let's ensure it
        expect(admin.legacy_user?).to be true
      end

      it 'ist false wenn die Behandlerin vor dem 3. Mai 2019 erstellt wurde und kein verschlüsseltes Passwort hat' do
        admin.update!(created_at: Time.zone.local(2019, 1, 1), crypted_password: nil)
        expect(admin.legacy_user?).to be false
      end

      it 'ist false wenn die Behandlerin nach dem 3. Mai 2019 erstellt wurde und ein verschlüsseltes Passwort hat' do
        admin.update!(created_at: Time.zone.local(2019, 5, 5))
        expect(admin.migrated_user?).to be false
      end
    end

    describe '#migrated_user?' do
      it 'ist false wenn wenn #legacy_user? true ist die Behandlerin aber noch keinen User assoziiert hat' do
        expect(admin.migrated_user?).to be false
      end

      it 'ist true wenn wenn #legacy_user? true ist und die Behandlerin einen User assoziert hat' do
        admin.migrate_to_user!('asdfasdf')
        expect(admin.migrated_user?).to be true
      end
    end

    describe '#migrate_to_user!' do
      it 'wirft einen LazyDeviseMigrationError mit "not_a_legacy_user", wenn es sich um keinen "legacy" Record handelt' do
        fresh_person = create(:physician, :not_legacy)
        expect { fresh_person.migrate_to_user!('passw') }.to raise_error(Person::LazyDeviseMigrationError, 'not_a_legacy_user')
      end

      it 'wirft einen LazyDeviseMigrationError mit "allready_migrated_user" wenn der "legacy" Record bereits migriert wurde' do
        migrated = admin.migrate_to_user!('passw')
        expect { migrated.migrate_to_user!('passw') }.to raise_error(Person::LazyDeviseMigrationError, 'allready_migrated_user')
      end

      it 'erstellt einen User record' do
        expect { admin.migrate_to_user!('asdfasdf') }.to change(User, :count).by(1)
      end

      it 'kopiert login, email und role auf die entsprechenden Felder des User Records' do
        migrated = admin.migrate_to_user!('asdfasdf')
        expect(migrated.user).to have_attributes(email: 'admin_1@example.com', username: 'admin_1', role: 'admin')
      end

      it 'gibt am ende der Ausführung seine Instanz zurück' do
        migrated = admin.migrate_to_user!('asdfasdf')
        expect(migrated.id).to eq admin.id
      end

      it 'setzt das korrekte Passwort auf dem neu erstellten User' do
        migrated = admin.migrate_to_user!('asdfasdf')
        expect(migrated.user.valid_password?('asdfasdf')).to be true
      end

      it 'erstellt einen User mit der Rolle "admin" für einen Physician mit der rolle "admin"' do
        migrated = admin.migrate_to_user!('asdfasdf')
        expect(migrated.user).to be_an_admin
      end

      it 'erstellt einen User mit der Rolle "assistant" für einen Assistant mit der rolle "assistant"' do
        assistant = create(:assistant, :legacy_user)
        migrated = assistant.migrate_to_user!('asdfasdf')
        expect(migrated.user).to be_an_assistant
      end

      it 'creates an physician user for a legacy physician role user' do
        physician = create(:physician, :legacy_user)
        migrated = physician.migrate_to_user!('asdfasdf')
        expect(migrated.user).to be_a_physician
      end

      it 'erstellt einen User mit der Rolle "bookkeeper" für einen Assistant mit der rolle "bookkeeper"' do
        bookkeeper = create(:bookkeeper, :legacy_user)
        migrated = bookkeeper.migrate_to_user!('asdfasdf')
        expect(migrated.user).to be_a_bookkeeper
      end

      it 'wirft ActiveRecord::RecordNotSaved wenn Assistant die Rolle "physician" hat' do
        assistant = create(:assistant, :legacy_user)
        assistant.update!(role: 7)
        expect { assistant.migrate_to_user!('asdfasdf') }.to raise_error(ActiveRecord::RecordNotSaved)
      end

      it 'wirft ActiveRecord::RecordNotSaved wenn Physician die Rolle "assistant" hat' do
        assistant = create(:physician, :legacy_user)
        assistant.update!(role: 5)
        expect { assistant.migrate_to_user!('asdfasdf') }.to raise_error(ActiveRecord::RecordNotSaved)
      end

      it 'wirft ActiveRecord::RecordNotSaved wenn Physician die Rolle "bookkeeper" hat' do
        assistant = create(:physician, :legacy_user)
        assistant.update!(role: 2)
        expect { assistant.migrate_to_user!('asdfasdf') }.to raise_error(ActiveRecord::RecordNotSaved)
      end
    end

    describe '#authenticate_login' do
      before do
        create(:assistant_legacy, login: 'assistant', email: 'assistant@mail.com')
      end

      it 'erstellt einen neuen User wenn gültige Parameter Benutzername und Passwort übergeben werden' do
        expect { Person.authenticate_login('assistant', 'Denteo12') }.to change(User, :count).by(1)
      end

      it 'erstellt einen neuen User wenn gültige Parameter Email und Passwort übergeben werden' do
        expect { Person.authenticate_login('assistant@mail.com', 'Denteo12') }.to change(User, :count).by(1)
      end

      it 'wirft LazyDeviseMigrationError mit "not_a_legacy_user" wenn es sich nicht um "legacy" Person handelt' do
        new_pers = create(:physician, :not_legacy, login: 'new_pers')
        new_pers.password = 'dent'
        new_pers.encrypt_password!
        expect { Person.authenticate_login('new_pers', 'dent') }.to raise_error(
          Person::LazyDeviseMigrationError, 'not_a_legacy_user'
        )
      end

      it 'wirft LazyDeviseMigrationError mit "allready_migrated_user" wenn Person bereits migriert wird' do
        Person.authenticate_login('assistant', 'Denteo12')
        expect { Person.authenticate_login('assistant', 'Denteo12') }.to raise_error(
          Person::LazyDeviseMigrationError, 'allready_migrated_user'
        )
      end

      it 'wirft AuthenticationError mit "wrong_password" wenn das Passwort nicht korrekt ist' do
        expect { Person.authenticate_login('assistant', 'wrong password') }.to raise_error(
          Person::AuthenticationError, 'wrong_password'
        )
      end

      it 'wirft AuthenticationError mit "missing_login_or_password" wenn kein Passwort übergeben wird' do
        expect { Person.authenticate_login('assistant', '') }.to raise_error(
          Person::AuthenticationError, 'missing_login_or_password'
        )
      end

      it 'wirft AuthenticationError mit "missing_login_or_password" wenn kein Benutzername übergeben wird' do
        expect { Person.authenticate_login('', 'Denteo12') }.to raise_error(
          Person::AuthenticationError, 'missing_login_or_password'
        )
      end

      it 'erzeugt einen User mit der korrekten Rolle' do
        migrated = Person.authenticate_login('assistant', 'Denteo12')
        expect(migrated.user).to be_an_assistant
      end
    end
  end

  describe '#physician?' do
    it 'gibt "false" zurück' do
      expect(Person.new.physician?).to be false
    end
  end

  describe '#assistant?' do
    it 'gibt "false" zurück' do
      expect(Person.new.assistant?).to be false
    end
  end

  describe '#authenticatable?' do
    it 'gibt "false" zurück' do
      expect(Person.new.authenticatable?).to be false
    end
  end
end
