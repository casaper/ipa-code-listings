require 'rails_helper'

RSpec.describe Physician, type: :model do
  context 'list positioning' do
    before do
      create_list(:physician, 2)
    end

    it 'updates the position correctly (before_save set_position)' do
      expect(Physician.pluck(:position)).to eq([1, 2])
      physician = create(:physician)
      expect(physician.position).to eq(3)
    end
  end

  it 'test_generate_display_name' do
    physician = create(:physician, prename: 'pre', name: 'name', title: 'Dr.')
    assert_equal 'pre name', physician.reload.display_name
  end

  describe '#physician?' do
    it 'gibt "true" zurück' do
      expect(Physician.new.physician?).to be true
    end
  end

  describe '#assistant?' do
    it 'gibt "false" zurück' do
      expect(Physician.new.assistant?).to be false
    end
  end

  describe '#authenticatable?' do
    it 'gibt "true" zurück' do
      expect(Physician.new.authenticatable?).to be true
    end
  end
end
