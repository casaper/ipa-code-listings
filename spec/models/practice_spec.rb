require 'rails_helper'

RSpec.describe Practice, type: :model do
  before do
    create(:practice)
  end

  describe '#pending_sms_notifications' do
    it 'should return empty relation' do
      expect(Practice.first.pending_sms_notifications).to eq([])
    end
  end

  describe '#physician?' do
    it 'gibt "false" zurück' do
      expect(Practice.new.physician?).to be false
    end
  end

  describe '#assistant?' do
    it 'gibt "false" zurück' do
      expect(Practice.new.assistant?).to be false
    end
  end

  describe '#authenticatable?' do
    it 'gibt "false" zurück' do
      expect(Practice.new.authenticatable?).to be false
    end
  end
end
