require 'rails_helper'

RSpec.describe User, type: :model do
  context 'validiert' do
    describe 'die Rolle' do
      context 'wenn person "Physician" ist' do
        subject(:user) { User.new(person: Physician.new) }

        it 'als valide, wenn sie "admin" ist' do
          user.role = 'admin'
          user.validate
          expect(user.errors.details).not_to include(:role)
        end

        it 'als valide, wenn sie "physician" ist' do
          user.role = 'physician'
          user.validate
          expect(user.errors.details).not_to include(:role)
        end

        it 'als nicht valide, wenn sie "assistant" ist' do
          user.role = 'assistant'
          user.validate
          expect(user.errors.details).to include(role: [{ error: :inclusion, value: 'assistant' }])
        end

        it 'als nicht valide, wenn sie "bookkeeper" ist' do
          user.role = 'bookkeeper'
          user.validate
          expect(user.errors.details).to include(role: [{ error: :inclusion, value: 'bookkeeper' }])
        end
      end

      context 'wenn person "Assistant" ist' do
        subject(:user) { User.new(person: create(:assistant), email: 'some@email.com') }

        it 'als valide, wenn sie "assistant" ist' do
          user.role = 'assistant'
          user.validate
          expect(user.errors.details).not_to include(:role)
        end

        it 'als valide, wenn sie "bookkeeper" ist' do
          user.role = 'bookkeeper'
          user.validate
          expect(user.errors.details).not_to include(:role)
        end

        it 'als valide, wenn sie "admin" ist' do
          user.role = 'admin'
          user.validate
          expect(user.errors.details).not_to include(:role)
        end

        it 'als nicht valide, wenn sie "physician" ist' do
          user.role = 'physician'
          user.validate
          expect(user.errors.details).to include(role: [{ error: :inclusion, value: 'physician' }])
        end
      end
    end

    describe 'die Relation person' do
      subject(:user) { User.new }

      it 'als valide, wenn person "Physician" ist' do
        user.person = Physician.new
        user.validate
        expect(user.errors.details).not_to include(:person)
      end

      it 'als valide, wenn person "Physician" ist' do
        user.person = Assistant.new
        user.validate
        expect(user.errors.details).not_to include(:person)
      end

      it 'als nicht valide, wenn person "Practice" ist' do
        user.person = Practice.new
        expect(user).not_to be_valid
        expect(user.errors.details).to include(person: [{ error: :wrong_type }])
      end

      it 'als nicht valide, wenn person "Guarantor" ist' do
        user.person = Guarantor.new
        expect(user).not_to be_valid
        expect(user.errors.details).to include(person: [{ error: :wrong_type }])
      end

      it 'als nicht valide, wenn person "Lab" ist' do
        user.person = Lab.new
        expect(user).not_to be_valid
        expect(user.errors.details).to include(person: [{ error: :wrong_type }])
      end

      it 'als nicht valide, wenn person "Patient" ist' do
        user.person = Patient.new
        expect(user).not_to be_valid
        expect(user.errors.details).to include(person: [{ error: :wrong_type }])
      end

      it 'als nicht valide, wenn person "Vendor" ist' do
        user.person = Vendor.new
        expect(user).not_to be_valid
        expect(user.errors.details).to include(person: [{ error: :wrong_type }])
      end

      it 'als nicht valide, wenn person "nil" ist' do
        user.person = nil
        expect(user).not_to be_valid
        expect(user.errors.details).to include(person: [{ error: :blank }])
      end
    end
  end


  describe 'delegiert' do
    subject(:user) do
      user = create(:user_admin)
      user.person.display_name = 'test_display_name'
      user.save!
      user
    end

    [
      :name_with_salutation, :formal_salutation, :display_name_with_short_salutation,
      :fullname, :label_dob, :zip_and_city, :sex, :short_name, :sms, :to_address_block_with_tel,
      :to_address_block, :salutation_display, :locale, :practice_id, :physician?, :assistant?,
      :physician_id, :kind, :api_token, :lang
    ].each do |delegation|
      it "das Attribute #{delegation} auf person" do
        expect(user).to respond_to(delegation)
      end
    end
  end

  describe 'hat die Methode #is_role?' do
    subject(:user) { create(:user_admin) }

    it 'die gleich reagiert wie die gleichnahmige Methode früher auf Person reagierte' do
      expect(user.is_role?('admin')).to be true
      expect(user.is_role?('physician')).to be false
    end
  end

  describe '#roles_collection gibt die Übersetzungs- Schlüssel-Paare für' do
    it '"admin" und "physician" zurück wenn person "Physician" ist' do
      user = User.new(person: Physician.new)
      expect(user.roles_collection).to include('Admin' => :admin, 'Physician' => :physician)
      expect(user.roles_collection).not_to include('Assistant' => :assistant, 'Bookkeeper' => :bookkeeper)
    end

    it '"admin", "assistant" und "bookkeeper" zurück wenn person "Assistant" ist' do
      user = User.new(person: Assistant.new)
      expect(user.roles_collection).to include('Admin' => :admin, 'Assistant' => :assistant, 'Bookkeeper' => :bookkeeper)
      expect(user.roles_collection).not_to include('Physician' => :physician)
    end
  end
end
