require 'rails_helper'

RSpec.describe Vendor, type: :model do
  describe '#physician?' do
    it 'gibt "false" zurück' do
      expect(Vendor.new.physician?).to be false
    end
  end

  describe '#assistant?' do
    it 'gibt "false" zurück' do
      expect(Vendor.new.assistant?).to be false
    end
  end

  describe '#authenticatable?' do
    it 'gibt "false" zurück' do
      expect(Vendor.new.authenticatable?).to be false
    end
  end
end
